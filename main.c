#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#define PI 3.14

int main()
{
    setlocale(LC_ALL, "Portuguese");
    printf("Hello mund�o!\n");

    float raio, area, p;

    printf("Informe o valor do raio de um c�rculo: ");
    scanf("%f", &raio);
    area = pow(raio, 2) * PI;
    printf("A �rea do c�rculo �: %2.2f\n", area);
    p = 2 * PI * raio;
    printf("O perim�tro do circulo informado �: %2.2f", p);
    return 0;
}
